import pygame
from helpers import *
from level import Level

def main():
    pygame.init()

    WIDTH = 1200
    HEIGHT = 600
    SIZE = (WIDTH, HEIGHT)
    BG = load_fig("background.png", SIZE)
    BG1 = load_fig("background1.jpg", SIZE)
    BG2 = load_fig("background2.jpg", SIZE)
    BG3 = load_fig("background3.jpg", SIZE)
    BG4 = load_fig("background4.jpg", SIZE)

    screen = pygame.display.set_mode(SIZE)
    # Definiranje sata za pracenje fps-a
    clock = pygame.time.Clock()

    best_score = 0
    current_level = 1
    level = Level(screen, current_level)


    done = False
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True

        # state processing
        if level.level_state == "play":
            if level.score >= 0 and level.score < 5:
                level.current_level = 1
                screen.blit(BG, (0, 0))
            elif level.score >= 5 and level.score < 10:
                level.current_level = 2
                screen.blit(BG1, (0, 0))
            elif level.score >= 10 and level.score < 15:
                level.current_level = 3
                screen.blit(BG2, (0, 0))
            elif level.score >= 15 and level.score < 20:
                level.current_level = 4
                screen.blit(BG3, (0, 0))
            elif level.score >= 20:
                level.current_level = 5
                screen.blit(BG4, (0, 0))
            level.update()

        elif level.level_state == "lost":
            if level.score > best_score:
                best_score = level.score
            current_level = 1
            level = Level(screen, current_level)
            level.show_menu()
        elif level.level_state == "menu":
            screen.blit(BG, (0, 0))
            level.show_menu()
        elif level.level_state == "highscore":
            screen.blit(BG, (0, 0))
            level.show_high_score(best_score)


        pygame.display.flip()
        clock.tick(60)


if __name__ == "__main__":
    main()
    pygame.quit()