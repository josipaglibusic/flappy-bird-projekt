import pygame
import random

class Entity(pygame.sprite.Sprite):
    """
    Main game entity
    Image is defined as a class level variable. Subclasses override their
    images from the level. Image loading is implemented inside the level
    """
    # Placeholder transparent image
    image = pygame.Surface((1, 1), pygame.SRCALPHA, 32)

    def __init__(self, level):
        """ Child classes have to configure their images before calling the
        super().__init__() method, otherwise the object will be initialized
        with a transparent image
        """

        self.level = level
        self.screen = level.screen
        self.sw, self.sh = self.screen.get_size()
        self.w, self.h = (100, 100)
        self.x, self.y = self.sw / 2, self.sh / 2
        self.vel = 1
        # One-off scale the image so that the mask is the same size as the images,
        # otherwise the mask will be the same size as the original image and will be
        # located at the upper left corner of the scaled image, which will break collision
        # detection.
        scaled_img = pygame.transform.scale(self.image, (self.w, self.h))
        self.rect = scaled_img.get_rect(center=(self.x, self.y))
        self.mask = pygame.mask.from_surface(scaled_img)
        self.drawn = None
        self.alive = True

    def handle_events(self):
        pass

    def update(self):
        self.handle_events()

    def draw(self):
        img = pygame.transform.scale(self.image, (self.w, self.h))
        self.rect = img.get_rect(center=(self.x, self.y))
        self.drawn = self.screen.blit(img, self.rect)
        self.mask = pygame.mask.from_surface(img)

class Player(Entity):
    GRAVITY = 0.5
    BIRD_VELOCITY = 3
    JUMP = -10

    def __init__(self, level):
        self.image = level.BIRD
        self.mask_img = level.BIRD
        super().__init__(level)
        self.w, self.h = (80, 80)
        self.x = 150

    def draw(self):
        img = pygame.transform.scale(self.image, (self.w, self.h))
        self.rect = img.get_rect(center = (self.x, self.y))
        self.drawn = self.screen.blit(img, self.rect)
        maskimg = pygame.transform.scale(self.mask_img, (self.w, self.h))
        self.mask = pygame.mask.from_surface(maskimg)

    def handle_events(self):
        self.fall()

        keys = pygame.key.get_pressed()
        if keys[pygame.K_SPACE]:
            self.jump()

    def fall(self):
        # Simuliranje eksponencijalne funkcije
        self.BIRD_VELOCITY += self.GRAVITY
        self.y += self.BIRD_VELOCITY

    def jump(self):
        self.BIRD_VELOCITY = self.JUMP

class PipePair(Entity):
    PIPE_VELOCITY = 4
    PIPE_DISTANCE_MIN = 180
    PIPE_DISTANCE_MAX = 250

    def __init__(self, level, startPosition):
        self.image = level.PIPE
        self.mask_img = level.PIPE
        super().__init__(level)
        self.x = startPosition
        self.w, self.h = (100, 300)
        self.top_offset = random.randint(self.PIPE_DISTANCE_MIN, self.PIPE_DISTANCE_MAX)
        self.bottom_offset = random.randint(self.PIPE_DISTANCE_MIN, self.PIPE_DISTANCE_MAX)

    def handle_events(self):
        self.move()

        # Kad cijev izade s ekrana, vrati ju nazad na pocetak
        if self.x <= -100:
            self.x = 1202
            self.top_offset = random.randint(self.PIPE_DISTANCE_MIN - self.level.current_level * 25, self.PIPE_DISTANCE_MAX - self.level.current_level * 25)
            self.bottom_offset = random.randint(self.PIPE_DISTANCE_MIN - self.level.current_level * 25, self.PIPE_DISTANCE_MAX - self.level.current_level * 25)

    def draw(self):
        topPipeImage = pygame.transform.scale(self.image, (self.w, self.h))
        topPipeImage = pygame.transform.rotate(topPipeImage, -180)
        self.topPipepRect = topPipeImage.get_rect(topright=(self.x, -self.top_offset))
        self.drawn = self.screen.blit(topPipeImage, self.topPipepRect)
        topPipeMaskimg = pygame.transform.scale(self.mask_img, (self.w, self.h))
        self.topPipeMask = pygame.mask.from_surface(topPipeMaskimg)

        bottomPipeImage = pygame.transform.scale(self.image, (self.w, self.h))
        self.bottomPipepRect = bottomPipeImage.get_rect(bottomright=(self.x, self.sh + self.bottom_offset))
        self.drawn = self.screen.blit(bottomPipeImage, self.bottomPipepRect)
        bottomPipeMaskimg = pygame.transform.scale(self.mask_img, (self.w, self.h))
        self.bottomPipeMask = pygame.mask.from_surface(bottomPipeMaskimg)

    def move(self):
        self.x -= self.PIPE_VELOCITY

    def collided_with(self, obj: Player):
        if self.topPipepRect.colliderect(obj.rect) or self.bottomPipepRect.colliderect(obj.rect):
            return True
        else:
            return False
