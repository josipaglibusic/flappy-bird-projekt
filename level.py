import pygame
from entities import Player, PipePair
from helpers import *

class Level(object):

    BIRD = pygame.image.load("bird.png")
    PIPE = pygame.image.load("pipe.png")

    def __init__(self, screen, level):
        self.current_level = level
        self.screen = screen
        self.score = 0
        self.sw, self.sh = screen.get_size()
        self.level_state = "menu" # play, won, lost
        self.player = Player(self)
        self.pipes = [PipePair(self, startPosition=2002), PipePair(self, startPosition=1710), PipePair(self, startPosition=1350), PipePair(self, startPosition=1110)]

    def update(self):
        self.player.update()
        self.player.draw()

        for pipe in self.pipes:
            pipe.update()
            pipe.draw()

        score_text = render_text("Score: %d"%self.score, 30)
        self.screen.blit(score_text, (20, 30))

        self.update_score()
        self.check_collision()


    def update_score(self):
        for pipe in self.pipes:
            if pipe.x == self.player.x:
                self.score += 1

    def check_collision(self):
        for pipe in self.pipes:
            if pipe.collided_with(self.player):
                self.level_state = "lost"

        if self.player.y <= 0 or self.player.y >= self.sh:
            self.level_state = "lost"


    def show_menu(self):
        mouse_x, mouse_y = pygame.mouse.get_pos()
        mouse_click = pygame.mouse.get_pressed()[0]

        menu_options = []

        start_game_obj = render_text("Start Game", 50)
        start_game_rect = start_game_obj.get_rect()
        start_game_rect.topleft = (480, 70)
        self.screen.blit(start_game_obj, start_game_rect)
        menu_options.append(start_game_rect)

        high_score_obj = render_text("High score", 50)
        high_score_rect = high_score_obj.get_rect()
        high_score_rect.topleft = (490, 140)
        self.screen.blit(high_score_obj, high_score_rect)
        menu_options.append(high_score_rect)

        quit_obj = render_text("Quit", 50)
        quit_rect = quit_obj.get_rect()
        quit_rect.topleft = (560, 210)
        self.screen.blit(quit_obj, quit_rect)
        menu_options.append(quit_rect)

        # Check for clicks
        if mouse_click:
            for i, rect in enumerate(menu_options):
                if rect.collidepoint((mouse_x, mouse_y)):
                    print(i)
                    if i == 0:
                        self.level_state = "play"
                    elif i == 1:
                        self.level_state = "highscore"
                    elif i == 2:
                        pygame.quit()

    def show_high_score(self, best_score: int):
        mouse_x, mouse_y = pygame.mouse.get_pos()
        mouse_click = pygame.mouse.get_pressed()[0]

        self.screen.blit(render_text("Best score: %d" % best_score, 50), (490, 140))

        back_obj = render_text("Back", 50)
        back_rect = back_obj.get_rect()
        back_rect.topleft = (560, 260)
        self.screen.blit(back_obj, back_rect)

        # Check for clicks
        if mouse_click:
            if back_rect.collidepoint((mouse_x, mouse_y)):
                self.level_state = "menu"