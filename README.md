# Flappy Bird Projekt

Projekt iz kolegija **Programsko Inženjerstvo**.

Za projekt sam se odlučila rekreirati igricu **Flappy Bird**.

Kontrole: **SPACE** -> Skok


Igrica se sastoji od:
- **Main menu** ekrana (Start game, High score, Quit opcije)
- **High score** ekrana koji prati zadnji najbolji score
- Glavni dio igre gdje kao igrač (ptica) pokušavate izbjeći cijevi kojima se udaljenost s vremenom sužava

## Neke stvari koje bi izdvojila

Kako bi rekreairala **Flappy Bird**, prvo sam morala implementirati slobodan pad za pticu. Simulirala sam eksponencijalnu funkciju kako bi dobila efekt slobodnog pada.

Nakon toga sam implementirala skok kako bi ptica mogla navigirati po levelu.

Pošto ptica zapravo stoji na mjestu (po X koordinati), morala sam cijevi pomicati unazad kako bi se dobio efekt kretanja kroz level.

Provjeru **kolizije** sam napravila izmedu cijevi i gornje i donje granice ekrana.

Igrica posjeduje koncept levela, svaki put kad score poraste za 5 (5, 10, 15, 20) level se poveća. Razliku između levela sam naglasila tako što sam izmjenila background i smanjila vertikalnu udaljenost između gornje i donje cijevi.